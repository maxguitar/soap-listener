<?php
/**
 * Handle SOAP calls and write their requests to log files.
 * 
 * @author Paul van der Meijs <paul@maxguitarstore.com>
 * @copyright Copyright (c) 2014, Paul van der Meijs
 * @version 0.0.1
 * @package maxguitar\soap\listener
 */

namespace maxguitar\soap\listener;
use SoapServer,
	SoapFault;

// Define the log directory constant
define("LOG_DIR", __DIR__ . "/logs");

// Write the request to a log file
if (false !== ($inh = fopen("php://input", "r")))
	file_put_contents(LOG_DIR . "/request-" . time() . ".log", call_user_func(function() use ($inh) {
		$ret = ["{$_SERVER["REQUEST_METHOD"]} {$_SERVER["REQUEST_URI"]} {$_SERVER["SERVER_PROTOCOL"]}"];

		foreach (getallheaders() as $key => $value) 
			$ret[] = "{$key}: {$value}";

		$ret[] = "";

		$ret[] = stream_get_contents($inh);

		return implode(PHP_EOL, $ret);
	}));

// Include the server class
include "Server.class.php";

// Create the server
try {
	$server = new SoapServer(null, array(
		"uri" => $_SERVER["REQUEST_URI"],
	));

	$server->setClass("maxguitar\\soap\\listener\\Server");
	
	$server->handle();

} catch (SoapFault $e) {
	var_dump($e);
}