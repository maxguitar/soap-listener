<?php
/**
 * Return the web store version number.
 * 
 * @author Paul van der Meijs <paul@maxguitarstore.com>
 * @copyright Copyright (c) 2014, Paul van der Meijs
 * @version 0.0.1
 * @package maxguitar\soap\listener
 */

namespace maxguitar\soap\listener;

return function($passkey) {
	return "3.1.11";
};