<?php
namespace maxguitar\soap\listener;

/**
 * Server
 * 
 * @author Paul van der Meijs <paul@maxguitarstore.com>
 * @copyright Copyright (c) 2014, Paul van der Meijs
 * @version 0.0.1
 * @package maxguitar\soap\listener
 */
class Server {

	/**
	 * Catch all method calls and write the given arguments to a log file. If a 
	 * closure for the called method is available in the functions folder it 
	 * will be executed and its return value returned.
	 * 
	 * @param string $name The name of the called method.
	 * @param array $args The arguments for the called method.
	 * @return mixed Returns the return value for from a closure if available or 
	 * NULL if not.
	 */
	public function __call($name, $args) {
		file_put_contents(LOG_DIR . "/function-{$name}-args-" . time() . ".log", print_r($args, true));

		if ($function = include "functions/{$name}.closure.php")
			return call_user_func_array($function, $args);
	}

}